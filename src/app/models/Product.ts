export class Product {
    name: string;
    description: string;
    src: string;

    constructor(name: string, description: string, src: string) {
        this.name = name;
        this.description = description;
        this.src = src
    }
}