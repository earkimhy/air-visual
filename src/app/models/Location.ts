export class Location {
    country: string;
    city: string;
    code: string;
    lat: number;
    lang: number;

    constructor() {}
}