export class CityAQI {
    city: string;
    country?: string;
    weather?: string;
    temperature?: number;
    aqi: number;
    pm2p5?: number;

    constructor(city: string, aqi: number, country?: string, weather?: string, temperature?: number, pm2p5?: number) {
        this.city = city;
        this.country = country
        this.weather = weather;
        this.temperature = temperature;
        this.aqi = aqi;
        this.pm2p5 = pm2p5;
    }

    getConditon(aqi: number) {
        let condition: string;
        if (aqi <= 50) {
            condition = 'Good'
        } else if (aqi <= 100) {
            condition = 'Moderate'
        } else if (aqi <= 150) {
            condition = 'Unhealthy for Sensitive Groups'
        } else if (aqi <= 200) {
            condition = 'Unhealthy'
        } else {
            condition = 'Very Unhealthy'
        }

        return condition;
    }

    getColor(aqi: number) {
        let color: string;
        if (aqi <= 50) {
            color = 'green'
        } else if (aqi <= 100) {
            color = 'yellow'
        } else if (aqi <= 150) {
            color = 'orange'
        } else if (aqi <= 200) {
            color = 'red'
        } else {
            color = 'purple'
        }

        return color;
    }
}