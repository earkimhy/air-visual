export class News {
    title: string;
    description: string;
    type: string;
    src: string;

    constructor(title: string, description: string, type: string, src: string) {
        this.title = title;
        this.description = description;
        this.type = type;
        this.src = src;
    }
}