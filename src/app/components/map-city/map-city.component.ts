import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { } from 'googlemaps';

@Component({
  selector: 'app-map-city',
  templateUrl: './map-city.component.html',
  styleUrls: ['./map-city.component.css']
})
export class MapCityComponent implements OnInit {

  @Input() location;

  @ViewChild('mapContainer', { static: true }) mapElement: any;
  map: google.maps.Map;

  constructor() { }

  ngOnInit() {

    const lat = this.location.lat;
    const lang = this.location.lang;

    const coordinates = new google.maps.LatLng(lat, lang);

    const mapProperties = {
      center: coordinates,
      zoom: 8
      // mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    const marker = new google.maps.Marker(
      {
        position: coordinates,
        map: this.map,
        label: '119'
      }
    )

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapProperties);
    marker.setMap(this.map);

  }

}
