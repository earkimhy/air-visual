import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CountryListAreaComponent } from './country-list-area.component';

describe('CountryListAreaComponent', () => {
  let component: CountryListAreaComponent;
  let fixture: ComponentFixture<CountryListAreaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CountryListAreaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CountryListAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
