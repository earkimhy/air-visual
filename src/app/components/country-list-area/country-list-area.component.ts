import { Component, OnInit } from '@angular/core';
import { Country } from 'src/app/models/Country';
import { default as countries } from "src/assets/countries.json";

@Component({
  selector: 'app-country-list-area',
  templateUrl: './country-list-area.component.html',
  styleUrls: ['./country-list-area.component.css']
})
export class CountryListAreaComponent implements OnInit {

  africas: Country[];
  asias: Country[];
  europes: Country[];
  northamericas: Country[];
  oceanias: Country[];
  southamericas: Country[];

  constructor() { }

  ngOnInit() {
    this.africas = countries.filter((country) => {
      return country.continent === 'Africa'
    })
    this.asias = countries.filter((country) => {
      return country.continent === 'Asia'
    })
    this.europes = countries.filter((country) => {
      return country.continent === 'Europe'
    })
    this.northamericas = countries.filter((country) => {
      return country.continent === 'North America'
    })
    this.oceanias = countries.filter((country) => {
      return country.continent === 'Oceania'
    })
    this.southamericas = countries.filter((country) => {
      return country.continent === 'South America'
    })
  }

}
