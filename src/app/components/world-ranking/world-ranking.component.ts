import { Component, OnInit } from '@angular/core';
import { CityAQI } from 'src/app/models/CityAQI';

@Component({
  selector: 'app-world-ranking',
  templateUrl: './world-ranking.component.html',
  styleUrls: ['./world-ranking.component.css']
})
export class WorldRankingComponent implements OnInit {

  cityAQIs: CityAQI[];

  constructor() { }

  ngOnInit() {
    this.cityAQIs = [
      new CityAQI('Lahore', 254, 'Pakistan'),
      new CityAQI('Dhaka', 253, 'Bangladesh'),
      new CityAQI('Delhi', 241, 'India'),
      new CityAQI('Hanoi', 192, 'Vietnam'),
      new CityAQI('Kabul', 179, 'Afghanistan'),
      new CityAQI('Kathmandu', 176, 'Nepal'),
      new CityAQI('Ulaanbaatar', 171, 'Mongolia'),
      new CityAQI('Yangon', 170, 'Myanmar'),
      new CityAQI('Mumbai', 165, 'India'),
      new CityAQI('Kuwait City', 165, 'Kuwait')
    ]
  }

}
