import { Component, OnInit } from '@angular/core';
import { News } from 'src/app/models/News';

@Component({
  selector: 'app-world-aq-news',
  templateUrl: './world-aq-news.component.html',
  styleUrls: ['./world-aq-news.component.css']
})
export class WorldAqNewsComponent implements OnInit {

  aqNews: News[];

  constructor() { }

  ngOnInit() {
    this.aqNews = [
      {
        title: 'Ho Chi Minh City air quality: Motorbike experiment reveals where the pollution hotspots are',
        description: '',
        type: 'Blog',
        src: 'dec13reportHCMCsmaller.webp'
      },
      {
        title: 'Pollution data just became available to millions more Pakistanis',
        description: '',
        type: 'News',
        src: 'nov20thumbnailbig.webp'
      },
      {
        title: 'AirVisual data: Hazardous Delhi pollution is the longest on record',
        description: '',
        type: 'New',
        src: 'delhithumbnailnov5.webp'
      }
    ]
  }

}
