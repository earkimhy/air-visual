import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorldAqNewsComponent } from './world-aq-news.component';

describe('WorldAqNewsComponent', () => {
  let component: WorldAqNewsComponent;
  let fixture: ComponentFixture<WorldAqNewsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorldAqNewsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorldAqNewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
