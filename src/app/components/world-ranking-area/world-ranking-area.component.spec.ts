import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorldRankingAreaComponent } from './world-ranking-area.component';

describe('WorldRankingAreaComponent', () => {
  let component: WorldRankingAreaComponent;
  let fixture: ComponentFixture<WorldRankingAreaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorldRankingAreaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorldRankingAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
