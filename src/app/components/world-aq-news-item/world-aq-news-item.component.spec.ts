import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorldAqNewsItemComponent } from './world-aq-news-item.component';

describe('WorldAqNewsItemComponent', () => {
  let component: WorldAqNewsItemComponent;
  let fixture: ComponentFixture<WorldAqNewsItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorldAqNewsItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorldAqNewsItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
