import { Component, OnInit, Input } from '@angular/core';
import { News } from 'src/app/models/News';

@Component({
  selector: 'app-world-aq-news-item',
  templateUrl: './world-aq-news-item.component.html',
  styleUrls: ['./world-aq-news-item.component.css']
})
export class WorldAqNewsItemComponent implements OnInit {

  @Input() aqNew: News;

  constructor() { }

  ngOnInit() {
  }

  setNewThumbnail() {
    return `../../../assets/img/${this.aqNew.src}`;
  }

}
