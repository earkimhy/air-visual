import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorldAqNewsAreaComponent } from './world-aq-news-area.component';

describe('WorldAqNewsAreaComponent', () => {
  let component: WorldAqNewsAreaComponent;
  let fixture: ComponentFixture<WorldAqNewsAreaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorldAqNewsAreaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorldAqNewsAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
