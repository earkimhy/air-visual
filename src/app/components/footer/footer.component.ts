import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  menus: Array<Object>;

  constructor() { }

  ngOnInit() {
    this.menus = [
      {
        menu: 'IQAir',
        submenus: [
          {
            name: 'About us',
            link: '#'
          },
          {
            name: 'Press',
            link: '#'
          },
          {
            name: 'Contact us',
            link: '#'
          },
          {
            name: 'Support',
            link: '#'
          },
          {
            name: 'Privacy Policy',
            link: '#'
          }
        ],
      },
      {
        menu: 'Explore',
        submenus: [
          {
            name: 'World air quality',
            link: '#'
          },
          {
            name: 'Air Insights',
            link: '#'
          },
          {
            name: 'AirVisual live map',
            link: '#'
          },
          {
            name: 'AirVisual Earth',
            link: '#'
          },
          {
            name: 'World city rankings',
            link: '#'
          }
        ]
      },
      {
        menu: 'Products & Solutions',
        submenus: [
          {
            name: 'AirVisual Enterprise',
            link: '#'
          },
          {
            name: 'Air Quality Monitor',
            link: '#'
          },
          {
            name: 'Air API',
            link: '#'
          },
          {
            name: 'Widget',
            link: '#'
          },
          {
            name: 'Air Quality App',
            link: '#'
          }
        ]
      },
    ]
  }

}
