import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AqiMeasurementTagComponent } from './aqi-measurement-tag.component';

describe('AqiMeasurementTagComponent', () => {
  let component: AqiMeasurementTagComponent;
  let fixture: ComponentFixture<AqiMeasurementTagComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AqiMeasurementTagComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AqiMeasurementTagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
