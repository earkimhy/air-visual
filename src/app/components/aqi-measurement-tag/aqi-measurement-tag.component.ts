import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-aqi-measurement-tag',
  templateUrl: './aqi-measurement-tag.component.html',
  styleUrls: ['./aqi-measurement-tag.component.css']
})
export class AqiMeasurementTagComponent implements OnInit {

  @Input() pm2p5: number;

  constructor() { }

  ngOnInit() {
  }

}
