import { Component, OnInit } from '@angular/core';
import { CityAQI } from 'src/app/models/CityAQI';

@Component({
  selector: 'app-city-cards',
  templateUrl: './city-cards.component.html',
  styleUrls: ['./city-cards.component.css']
})
export class CityCardsComponent implements OnInit {

  cityAQIs: CityAQI[];

  constructor() { }

  ngOnInit() {
    this.cityAQIs = [
      new CityAQI('Beijing', 155, 'China', 'night-clear-sky', -4, 64),
      new CityAQI('Seoul', 102, 'Korea', 'scattered-clouds', -1, 36),
      new CityAQI('Los Angeles', 40, 'USA', 'clear-sky', 21, 9.7)
    ]
  }

}
