import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CityAqiComponent } from './city-aqi.component';

describe('CityAqiComponent', () => {
  let component: CityAqiComponent;
  let fixture: ComponentFixture<CityAqiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CityAqiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CityAqiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
