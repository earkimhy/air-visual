import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocationService } from 'src/app/services/location.service'

import { Location } from 'src/app/models/Location'

@Component({
  selector: 'app-city-aqi',
  templateUrl: './city-aqi.component.html',
  styleUrls: ['./city-aqi.component.css']
})

export class CityAqiComponent implements OnInit {
  path: string;
  location: Location;
  constructor(private router: Router, private locationService: LocationService) { }

  ngOnInit() {
    // get the country and city from the path
    this.path = this.router.url
    const paths = this.router.url.split('/')

    this.locationService.getLocation(paths[2]).subscribe(location => {
      this.location = location
      console.log(location)
    })

    console.log(this.location)

    // check if the country and the city exist in the data
    // const location = locations.filter((location) => {
    //   return this.trim(location.country).toLowerCase() === paths[1]
    //     && this.trim(location.city).toLowerCase() === paths[2]
    // })

    // this.location = location[0]
  }

  trim(string: string) {
    return string.replace(/\s/g, '')
  }

  // getCities() {
  //   return [
  //     {
  //       'country': 'China',
  //       'city': 'Beijing',
  //       'lat': 39.916668,
  //       'lang': 116.383331
  //     },
  //     {
  //       'country': 'Cambodia',
  //       'city': 'Phnom Penh',
  //       'lat': 11.562108,
  //       'lang': 104.888535
  //     }
  //   ]
  // }

}
