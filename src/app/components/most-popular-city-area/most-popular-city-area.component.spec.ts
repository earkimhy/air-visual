import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MostPopularCityAreaComponent } from './most-popular-city-area.component';

describe('MostPopularCityAreaComponent', () => {
  let component: MostPopularCityAreaComponent;
  let fixture: ComponentFixture<MostPopularCityAreaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MostPopularCityAreaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MostPopularCityAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
