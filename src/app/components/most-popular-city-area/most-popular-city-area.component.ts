import { Component, OnInit } from '@angular/core';
import { CityAQI } from 'src/app/models/CityAQI';

@Component({
  selector: 'app-most-popular-city-area',
  templateUrl: './most-popular-city-area.component.html',
  styleUrls: ['./most-popular-city-area.component.css']
})
export class MostPopularCityAreaComponent implements OnInit {

  cityAQIs: CityAQI[];

  constructor() { }

  ngOnInit() {
    this.cityAQIs = [
      new CityAQI('Bangkok', 221),
      new CityAQI('Chiang Mai', 199),
      new CityAQI('Phuket', 221),
      new CityAQI('Nakhon Ratchasima', 212),
      new CityAQI('Seoul', 40),
      new CityAQI('Beijing', 33),
      new CityAQI('Los Angeles', 221),
      new CityAQI('Ho Chi Minh City', 79),
      new CityAQI('Hanoi', 200),
      new CityAQI('Paris', 150),
      new CityAQI('Shanghai', 190),
      new CityAQI('Jakarta', 211),
      new CityAQI('Delhi', 39),
      new CityAQI('Mumbai', 50),
      new CityAQI('Busan', 40),
      new CityAQI('Kolkata', 90),
      new CityAQI('Daegu', 22),
      new CityAQI('Chengdu', 21),
      new CityAQI('Guangzhou', 33),
      new CityAQI('Medam', 21),
      new CityAQI('Batam', 32),
      new CityAQI('New York', 90),
      new CityAQI('Sydney', 89),
      new CityAQI('Denver', 231),
      new CityAQI('Newcastle', 89),
      new CityAQI('Wollongong', 87),
      new CityAQI('Taipei', 231),
      new CityAQI('Tokyo', 233),
      new CityAQI('Singapore', 23),
      new CityAQI('London', 120),
    ]
  }

}
