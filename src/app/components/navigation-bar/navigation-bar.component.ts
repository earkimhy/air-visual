import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navigation-bar',
  templateUrl: './navigation-bar.component.html',
  styleUrls: ['./navigation-bar.component.css']
})
export class NavigationBarComponent implements OnInit {

  menuListItems:Array<Object>;
  
  constructor() { }

  ngOnInit() {
    this.menuListItems = [
      {
        name: 'explore',
        link: '#',
      },
      {
        name: 'products',
        link: '#',
      },
      {
        name: 'community',
        link: '#',
      },
      {
        name: 'solutions',
        link: '#',
      },
    ]
  }

}
