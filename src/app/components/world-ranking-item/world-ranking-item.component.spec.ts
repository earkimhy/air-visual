import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorldRankingItemComponent } from './world-ranking-item.component';

describe('WorldRankingItemComponent', () => {
  let component: WorldRankingItemComponent;
  let fixture: ComponentFixture<WorldRankingItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorldRankingItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorldRankingItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
