import { Component, OnInit, Input } from '@angular/core';
import { CityAQI } from 'src/app/models/CityAQI';

@Component({
  selector: 'app-world-ranking-item',
  templateUrl: './world-ranking-item.component.html',
  styleUrls: ['./world-ranking-item.component.css']
})
export class WorldRankingItemComponent implements OnInit {

  @Input() cityAQI: CityAQI;
  @Input() index: number;

  constructor() { }

  ngOnInit() {
  }

  setFlagIcon() {
    return `../../../assets/img/${this.cityAQI.country.toLowerCase()}.jpg`
  }

  setAQIBackgroundClass() {
    return `bg-${this.cityAQI.getColor(this.cityAQI.aqi)}`
  }

}
