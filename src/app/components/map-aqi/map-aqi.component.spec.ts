import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapAqiComponent } from './map-aqi.component';

describe('MapAqiComponent', () => {
  let component: MapAqiComponent;
  let fixture: ComponentFixture<MapAqiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapAqiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapAqiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
