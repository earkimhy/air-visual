import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-map-aqi',
  templateUrl: './map-aqi.component.html',
  styleUrls: ['./map-aqi.component.css']
})
export class MapAqiComponent implements OnInit {

  @Input() location: Object;
  
  constructor() { }

  ngOnInit() {
  }

}
