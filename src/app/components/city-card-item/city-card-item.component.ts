import { Component, OnInit, Input } from '@angular/core';
import { CityAQI } from 'src/app/models/CityAQI';

@Component({
  selector: 'app-city-card-item',
  templateUrl: './city-card-item.component.html',
  styleUrls: ['./city-card-item.component.css']
})
export class CityCardComponent implements OnInit {

  @Input() cityAQI: CityAQI;
  color: string;

  constructor() { }

  ngOnInit() {
    this.color = this.cityAQI.getColor(this.cityAQI.aqi)
  }

  // set weather image
  setWeatherImage() {
    return `../../../assets/img/${this.cityAQI.weather}.svg`
  }

  //set image by color
  setFaceImage() {
    return `../../../assets/img/ic-face-${this.color}.svg`
  }

  setCardBodyClasses() {

    return `bg-${this.color}`
  }

  setCardInfoClasses() {
    const color = this.cityAQI.getColor(this.cityAQI.aqi)
    return `info-${this.color}`
  }

}
