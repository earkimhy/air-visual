import { Component, OnInit, Input } from '@angular/core';
import { CityAQI } from 'src/app/models/CityAQI';

@Component({
  selector: 'app-city-tag',
  templateUrl: './city-tag.component.html',
  styleUrls: ['./city-tag.component.css']
})
export class CityTagComponent implements OnInit {

  @Input() cityAQI: CityAQI;

  constructor() { }

  ngOnInit() {
  }

  setDotBackgroundColor() {
    return `bg-${this.cityAQI.getColor(this.cityAQI.aqi)}`
  }

}
