import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CityTagComponent } from './city-tag.component';

describe('CityTagComponent', () => {
  let component: CityTagComponent;
  let fixture: ComponentFixture<CityTagComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CityTagComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CityTagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
