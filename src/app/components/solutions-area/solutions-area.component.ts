import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/models/Product';

@Component({
  selector: 'app-solutions-area',
  templateUrl: './solutions-area.component.html',
  styleUrls: ['./solutions-area.component.css']
})
export class SolutionsAreaComponent implements OnInit {

  products: Product[];

  constructor() { }

  ngOnInit() {
    this.products = [
      new Product('AirVisual App', 'Free iOS and Android air quality app', 'airvisual-app-banner.webp'),
      new Product('AirVisual Monitor', 'Best indoor and outoor air quality monitor', 'airvisual-pro-banner.webp'),
      new Product('AirVisual API', 'Real-time air quality API for accurate global data', 'api-banner.webp')
    ]
  }

}
