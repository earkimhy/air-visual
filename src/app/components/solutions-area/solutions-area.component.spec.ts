import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolutionsAreaComponent } from './solutions-area.component';

describe('SolutionsAreaComponent', () => {
  let component: SolutionsAreaComponent;
  let fixture: ComponentFixture<SolutionsAreaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolutionsAreaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolutionsAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
