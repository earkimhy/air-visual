import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

import { Location } from 'src/app/models/Location'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  baseUrl: string = 'http://localhost:3000'

  constructor(private http: HttpClient) { }

  getLocation(code: string): Observable<Location> {
    return this.http.get<Location>(`${this.baseUrl}/locations/${code}`)
  }
}
