import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CityAqiComponent } from './components/city-aqi/city-aqi.component';
import { IndexComponent } from './components/index/index.component';


const routes: Routes = [
  {
    path: '',
    component: IndexComponent
  },
  {
    path: ':country_id/:id',
    component: CityAqiComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
