import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { NavigationBarComponent } from './components/navigation-bar/navigation-bar.component';
import { ExploreComponent } from './components/explore/explore.component';
import { CityCardComponent } from './components/city-card-item/city-card-item.component';
import { ExploreAreaComponent } from './components/explore-area/explore-area.component';
import { CityCardsComponent } from './components/city-cards/city-cards.component';
import { WorldRankingAreaComponent } from './components/world-ranking-area/world-ranking-area.component';
import { WorldRankingComponent } from './components/world-ranking/world-ranking.component';
import { WorldRankingItemComponent } from './components/world-ranking-item/world-ranking-item.component';
import { SolutionsAreaComponent } from './components/solutions-area/solutions-area.component';
import { SolutionItemComponent } from './components/solution-item/solution-item.component';
import { WorldAqNewsAreaComponent } from './components/world-aq-news-area/world-aq-news-area.component';
import { WorldAqNewsItemComponent } from './components/world-aq-news-item/world-aq-news-item.component';
import { WorldAqNewsComponent } from './components/world-aq-news/world-aq-news.component';
import { MostPopularCityAreaComponent } from './components/most-popular-city-area/most-popular-city-area.component';
import { CityTagComponent } from './components/city-tag/city-tag.component';
import { CountryListAreaComponent } from './components/country-list-area/country-list-area.component';
import { FooterComponent } from './components/footer/footer.component';
import { BreadcrumbComponent } from './components/breadcrumb/breadcrumb.component';
import { LocateMeComponent } from './components/locate-me/locate-me.component';
import { PageTitleComponent } from './components/page-title/page-title.component';
import { FollowButtonComponent } from './components/follow-button/follow-button.component';
import { ShareButtonComponent } from './components/share-button/share-button.component';
import { MapAqiComponent } from './components/map-aqi/map-aqi.component';
import { AqiMeasurementTagComponent } from './components/aqi-measurement-tag/aqi-measurement-tag.component';
import { MapCityComponent } from './components/map-city/map-city.component';
import { CityAqiComponent } from './components/city-aqi/city-aqi.component';
import { IndexComponent } from './components/index/index.component';
import { from } from 'rxjs';

@NgModule({
  declarations: [
    AppComponent,
    NavigationBarComponent,
    ExploreComponent,
    CityCardComponent,
    ExploreAreaComponent,
    CityCardsComponent,
    WorldRankingAreaComponent,
    WorldRankingComponent,
    WorldRankingItemComponent,
    SolutionsAreaComponent,
    SolutionItemComponent,
    WorldAqNewsAreaComponent,
    WorldAqNewsItemComponent,
    WorldAqNewsComponent,
    MostPopularCityAreaComponent,
    CityTagComponent,
    CountryListAreaComponent,
    FooterComponent,
    BreadcrumbComponent,
    LocateMeComponent,
    PageTitleComponent,
    FollowButtonComponent,
    ShareButtonComponent,
    MapAqiComponent,
    AqiMeasurementTagComponent,
    MapCityComponent,
    CityAqiComponent,
    IndexComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
